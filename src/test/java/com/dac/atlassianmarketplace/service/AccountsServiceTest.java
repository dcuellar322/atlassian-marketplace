package com.dac.atlassianmarketplace.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import com.dac.atlassianmarketplace.exception.AccountNotFoundException;
import com.dac.atlassianmarketplace.repository.AccountRepository;
import com.dac.atlassianmarketplace.repository.entity.Account;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

class AccountsServiceTest {

    @Mock
    private AccountRepository accountRepositoryMock;

    @InjectMocks
    private AccountsService underTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    /**
     * Test for {@link AccountsService#createAccount(Account)}.
     */
    @Test
    void testCreateAccount() {
        Account expected = mock(Account.class);

        when(accountRepositoryMock.save(expected)).thenReturn(expected);

        Account actual = underTest.createAccount(expected);

        assertEquals(expected, actual);
        verify(accountRepositoryMock, times(1)).save(expected);
        verifyNoMoreInteractions(accountRepositoryMock);
    }

    /**
     * Test for {@link AccountsService#getAccount(Long)}.
     * <p>
     * Case for when account found.
     */
    @Test
    void testGetAccount_AccountFound() {
        Long id = 1L;
        Optional<Account> expectedAccount = Optional.of(new Account("abc", "a1", "a2", "city", "state", "zip", "country"));

        when(accountRepositoryMock.findById(id)).thenReturn(expectedAccount);

        Optional<Account> actualAccount = underTest.getAccount(id);

        assertTrue(actualAccount.isPresent());
        assertEquals(expectedAccount.get().getCompanyName(), actualAccount.get().getCompanyName());
        assertEquals(expectedAccount.get().getAddressLine1(), actualAccount.get().getAddressLine1());
        assertEquals(expectedAccount.get().getAddressLine2(), actualAccount.get().getAddressLine2());
        assertEquals(expectedAccount.get().getCity(), actualAccount.get().getCity());
        assertEquals(expectedAccount.get().getState(), actualAccount.get().getState());
        assertEquals(expectedAccount.get().getPostalCode(), actualAccount.get().getPostalCode());
        assertEquals(expectedAccount.get().getCountry(), actualAccount.get().getCountry());

        verify(accountRepositoryMock, times(1)).findById(id);
        verifyNoMoreInteractions(accountRepositoryMock);
    }

    /**
     * Test for {@link AccountsService#getAccount(Long)}.
     * <p>
     * Case for when account not found.
     */
    @Test
    void testGetAccount_AccountNotFound() {
        Long id = 2L;
        Optional<Account> expectedAccount = Optional.empty();

        when(accountRepositoryMock.findById(id)).thenReturn(expectedAccount);

        Optional<Account> actualAccount = underTest.getAccount(id);

        assertFalse(actualAccount.isPresent());

        verify(accountRepositoryMock, times(1)).findById(id);
        verifyNoMoreInteractions(accountRepositoryMock);
    }

    /**
     * Test for {@link AccountsService#getAccounts()}.
     */
    @Test
    void testGetAccounts() {
        List<Account> expectedAccounts = Arrays.asList(
                new Account("abc", "a1", "a2", "city", "state", "zip", "country"),
                new Account("def", "b1", null, "c1", "state", "zip", "country")
        );

        when(accountRepositoryMock.findAll()).thenReturn(expectedAccounts);

        List<Account> actualAccounts = underTest.getAccounts();

        assertEquals(expectedAccounts.size(), actualAccounts.size());
        IntStream.range(0, expectedAccounts.size())
                .forEach(i -> {
                    assertEquals(expectedAccounts.get(i).getCompanyName(), actualAccounts.get(i).getCompanyName());
                    assertEquals(expectedAccounts.get(i).getAddressLine1(), actualAccounts.get(i).getAddressLine1());
                    assertEquals(expectedAccounts.get(i).getAddressLine2(), actualAccounts.get(i).getAddressLine2());
                    assertEquals(expectedAccounts.get(i).getCity(), actualAccounts.get(i).getCity());
                    assertEquals(expectedAccounts.get(i).getState(), actualAccounts.get(i).getState());
                    assertEquals(expectedAccounts.get(i).getPostalCode(), actualAccounts.get(i).getPostalCode());
                    assertEquals(expectedAccounts.get(i).getCountry(), actualAccounts.get(i).getCountry());
                });

        verify(accountRepositoryMock, times(1)).findAll();
        verifyNoMoreInteractions(accountRepositoryMock);
    }

    /**
     * Test for {@link AccountsService#updateAccount(Long, Account)}.
     * <p>
     * Case for when account found.
     */
    @Test
    void testUpdateAccount_AccountFound() {
        Long id = 1L;
        Account originalAccount = mock(Account.class);
        Account updatedAccount = new Account("def", "b1", null, "c1", "state", "zip", "country");

        when(accountRepositoryMock.findById(id)).thenReturn(Optional.of(originalAccount));
        when(accountRepositoryMock.save(any(Account.class))).thenReturn(updatedAccount);

        underTest.updateAccount(id, updatedAccount);

        verify(accountRepositoryMock, times(1)).findById(id);
        verify(originalAccount, times(1)).setCompanyName(updatedAccount.getCompanyName());
        verify(originalAccount, times(1)).setAddressLine1(updatedAccount.getAddressLine1());
        verify(originalAccount, times(1)).setAddressLine2(updatedAccount.getAddressLine2());
        verify(originalAccount, times(1)).setCity(updatedAccount.getCity());
        verify(originalAccount, times(1)).setState(updatedAccount.getState());
        verify(originalAccount, times(1)).setPostalCode(updatedAccount.getPostalCode());
        verify(originalAccount, times(1)).setCountry(updatedAccount.getCountry());
        verify(accountRepositoryMock, times(1)).save(any(Account.class));

        verifyNoMoreInteractions(accountRepositoryMock);
        verifyNoMoreInteractions(originalAccount);
    }

    /**
     * Test for {@link AccountsService#updateAccount(Long, Account)}.
     * <p>
     * Case for when account not found.
     */
    @Test
    void testUpdateAccount_AccountNotFound() {
        Long id = 2L;

        when(accountRepositoryMock.findById(id)).thenReturn(Optional.empty());

        try {
            underTest.updateAccount(id, any(Account.class));
            fail();
        } catch (Exception ex) {
            assertTrue(ex instanceof AccountNotFoundException);
            assertEquals(String.format("Could not find account %d", id), ex.getMessage());
        }

        verify(accountRepositoryMock, times(1)).findById(id);
        verify(accountRepositoryMock, times(0)).save(any(Account.class));
        verifyNoMoreInteractions(accountRepositoryMock);
    }
}