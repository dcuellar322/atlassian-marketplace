package com.dac.atlassianmarketplace.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import com.dac.atlassianmarketplace.exception.AccountNotFoundException;
import com.dac.atlassianmarketplace.exception.ContactNotFoundException;
import com.dac.atlassianmarketplace.repository.AccountRepository;
import com.dac.atlassianmarketplace.repository.ContactRepository;
import com.dac.atlassianmarketplace.repository.entity.Account;
import com.dac.atlassianmarketplace.repository.entity.Contact;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

class ContactsServiceTest {

    @Mock
    private ContactRepository contactRepositoryMock;

    @Mock
    private AccountRepository accountRepositoryMock;

    @InjectMocks
    private ContactsService underTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    /**
     * Test for {@link ContactsService#associateContactWithAccount(Long, Long)}.
     * <p>
     * Case for when both account and contact are found.
     */
    @Test
    void testAssociateContactWithAccount_AccountAndContactFound() {
        Long id = 2L;
        Long accountId = 99L;

        Account account = mock(Account.class);
        when(accountRepositoryMock.findById(accountId)).thenReturn(Optional.of(account));

        Contact contact = mock(Contact.class);
        when(contactRepositoryMock.findById(id)).thenReturn(Optional.of(contact));

        when(contactRepositoryMock.save(any(Contact.class))).thenReturn(contact);

        underTest.associateContactWithAccount(id, accountId);

        verify(accountRepositoryMock, times(1)).findById(accountId);
        verify(contactRepositoryMock, times(1)).findById(id);

        verify(contact).setAccount(account);
        verify(contactRepositoryMock).save(contact);

        verifyNoMoreInteractions(accountRepositoryMock);
        verifyNoMoreInteractions(contactRepositoryMock);
        verifyNoMoreInteractions(contact);
    }

    /**
     * Test for {@link ContactsService#associateContactWithAccount(Long, Long)}.
     * <p>
     * Case for when account is not found.
     */
    @Test
    void testAssociateContactWithAccount_AccountNotFound() {
        Long id = 2L;
        Long accountId = 99L;

        when(accountRepositoryMock.findById(accountId)).thenReturn(Optional.empty());

        try {
            underTest.associateContactWithAccount(id, accountId);
            fail();
        } catch (Exception ex) {
            assertTrue(ex instanceof AccountNotFoundException);
            assertEquals(String.format("Could not find account %d", accountId), ex.getMessage());
        }

        verify(accountRepositoryMock, times(1)).findById(accountId);

        verifyNoMoreInteractions(accountRepositoryMock);
        verifyNoInteractions(contactRepositoryMock);
    }

    /**
     * Test for {@link ContactsService#associateContactWithAccount(Long, Long)}.
     * <p>
     * Case for when contact is not found.
     */
    @Test
    void testAssociateContactWithAccount_ContactNotFound() {
        Long id = 2L;
        Long accountId = 99L;

        Account account = mock(Account.class);
        when(accountRepositoryMock.findById(accountId)).thenReturn(Optional.of(account));
        when(contactRepositoryMock.findById(id)).thenReturn(Optional.empty());

        try {
            underTest.associateContactWithAccount(id, accountId);
            fail();
        } catch (Exception ex) {
            assertTrue(ex instanceof ContactNotFoundException);
            assertEquals(String.format("Could not find contact %d", id), ex.getMessage());
        }

        verify(accountRepositoryMock, times(1)).findById(accountId);
        verify(contactRepositoryMock, times(1)).findById(id);

        verifyNoMoreInteractions(accountRepositoryMock);
        verifyNoMoreInteractions(contactRepositoryMock);
    }

    /**
     * Test for {@link ContactsService#createContact(Contact)}.
     */
    @Test
    void testCreateContact() {
        Contact expected = mock(Contact.class);

        when(contactRepositoryMock.save(expected)).thenReturn(expected);

        Contact actual = underTest.createContact(expected);

        assertEquals(expected, actual);
        verify(contactRepositoryMock, times(1)).save(expected);
        verifyNoMoreInteractions(contactRepositoryMock);
    }

    /**
     * Test for {@link ContactsService#getContact(Long)}.
     * <p>
     * Case for when contact found.
     */
    @Test
    void testGetContact_ContactFound() {
        Long id = 1L;
        Optional<Contact> expectedContact = Optional.of(new Contact("abc", "a1@test.com", null, null, "state", "zip", "country", "us", null));

        when(contactRepositoryMock.findById(id)).thenReturn(expectedContact);

        Optional<Contact> actualContact = underTest.getContact(id);

        assertTrue(actualContact.isPresent());
        assertEquals(expectedContact.get().getName(), actualContact.get().getName());
        assertEquals(expectedContact.get().getEmailAddress(), actualContact.get().getEmailAddress());
        assertEquals(expectedContact.get().getAddressLine1(), actualContact.get().getAddressLine1());
        assertEquals(expectedContact.get().getAddressLine2(), actualContact.get().getAddressLine2());
        assertEquals(expectedContact.get().getCity(), actualContact.get().getCity());
        assertEquals(expectedContact.get().getState(), actualContact.get().getState());
        assertEquals(expectedContact.get().getPostalCode(), actualContact.get().getPostalCode());
        assertEquals(expectedContact.get().getCountry(), actualContact.get().getCountry());
        assertEquals(expectedContact.get().getAccount(), actualContact.get().getAccount());

        verify(contactRepositoryMock, times(1)).findById(id);
        verifyNoMoreInteractions(contactRepositoryMock);
    }

    /**
     * Test for {@link ContactsService#getContact(Long)}.
     * <p>
     * Case for when contact not found.
     */
    @Test
    void testGetContact_ContactNotFound() {
        Long id = 2L;

        when(accountRepositoryMock.findById(id)).thenReturn(Optional.empty());

        Optional<Contact> actualContact = underTest.getContact(id);

        assertFalse(actualContact.isPresent());

        verify(contactRepositoryMock, times(1)).findById(id);
        verifyNoMoreInteractions(contactRepositoryMock);
    }

    /**
     * Test for {@link ContactsService#getContacts()}.
     */
    @Test
    void testGetContacts() {
        List<Contact> expectedContacts = Arrays.asList(
                new Contact("abc", "a1@test.com", null, null, "state", "zip", "country", "us", null),
                new Contact("def", "b1@test.com", null, null, "state", "zip", "country", "us", null)
        );

        when(contactRepositoryMock.findAll()).thenReturn(expectedContacts);

        List<Contact> actualContacts = underTest.getContacts();

        verifyContacts(expectedContacts, actualContacts);
        verify(contactRepositoryMock, times(1)).findAll();
        verifyNoMoreInteractions(contactRepositoryMock);
    }

    /**
     * Test for {@link ContactsService#getContactsByAccountId(Long)}.
     * <p>
     * Case for when account found.
     */
    @Test
    void testGetContactsByAccountId_AccountFound() {
        Long accountId = 99L;

        List<Contact> expectedContacts = Arrays.asList(
                new Contact("abc", "a1@test.com", null, null, "state", "zip", "country", "us", null),
                new Contact("def", "b1@test.com", null, null, "state", "zip", "country", "us", null)
        );

        when(accountRepositoryMock.existsById(accountId)).thenReturn(true);
        when(contactRepositoryMock.findByAccountId(accountId)).thenReturn(expectedContacts);

        List<Contact> actualContacts = underTest.getContactsByAccountId(accountId);

        verifyContacts(expectedContacts, actualContacts);
    }

    /**
     * Test for {@link ContactsService#getContactsByAccountId(Long)}.
     * <p>
     * Case for when account not found.
     */
    @Test
    void testGetContactsByAccountId_AccountNotFound() {
        Long accountId = 99L;

        when(accountRepositoryMock.existsById(accountId)).thenReturn(false);

        try {
            underTest.getContactsByAccountId(accountId);
            fail();
        } catch (Exception ex) {
            assertTrue(ex instanceof AccountNotFoundException);
            assertEquals(String.format("Could not find account %d", accountId), ex.getMessage());
        }

        verify(accountRepositoryMock).existsById(accountId);
        verifyNoMoreInteractions(accountRepositoryMock);
        verifyNoInteractions(contactRepositoryMock);
    }

    /**
     * Test for {@link ContactsService#getContactByIdAndAccountId(Long, Long)}.
     * <p>
     * Case for when account not found.
     */
    @Test
    void testGetContactsByIdAndAccountId_AccountNotFound() {
        Long id = 2L;
        Long accountId = 99L;

        when(accountRepositoryMock.existsById(accountId)).thenReturn(false);

        try {
            underTest.getContactByIdAndAccountId(id, accountId);
            fail();
        } catch (Exception ex) {
            assertTrue(ex instanceof AccountNotFoundException);
            assertEquals(String.format("Could not find account %d", accountId), ex.getMessage());
        }

        verify(accountRepositoryMock).existsById(accountId);
        verifyNoMoreInteractions(accountRepositoryMock);
        verifyNoInteractions(contactRepositoryMock);
    }

    /**
     * Test for {@link ContactsService#getContactByIdAndAccountId(Long, Long)}.
     * <p>
     * Case for when contact not found.
     */
    @Test
    void testGetContactsByIdAndAccountId_ContactNotFound() {
        Long id = 2L;
        Long accountId = 99L;

        when(accountRepositoryMock.existsById(accountId)).thenReturn(true);
        when(contactRepositoryMock.findByIdAndAccountId(id, accountId)).thenReturn(Optional.empty());

        Optional<Contact> actualContact = underTest.getContactByIdAndAccountId(id, accountId);

        assertFalse(actualContact.isPresent());

        verify(accountRepositoryMock).existsById(accountId);
        verify(contactRepositoryMock).findByIdAndAccountId(id, accountId);
        verifyNoMoreInteractions(accountRepositoryMock);
        verifyNoMoreInteractions(contactRepositoryMock);
    }

    /**
     * Test for {@link ContactsService#getContactByIdAndAccountId(Long, Long)}.
     * <p>
     * Case for when both account and contact found.
     */
    @Test
    void testGetContactsByIdAndAccountId_AccountAndContactFound() {
        Long id = 2L;
        Long accountId = 99L;

        Contact expectedContact = mock(Contact.class);
        when(accountRepositoryMock.existsById(accountId)).thenReturn(true);
        when(contactRepositoryMock.findByIdAndAccountId(id, accountId)).thenReturn(Optional.of(expectedContact));

        Optional<Contact> actualContact = underTest.getContactByIdAndAccountId(id, accountId);

        assertTrue(actualContact.isPresent());
        assertEquals(expectedContact, actualContact.get());

        verify(accountRepositoryMock).existsById(accountId);
        verify(contactRepositoryMock).findByIdAndAccountId(id, accountId);
        verifyNoMoreInteractions(accountRepositoryMock);
        verifyNoMoreInteractions(contactRepositoryMock);
    }

    /**
     * Test for {@link ContactsService#updateContact(Long, Contact)}.
     * <p>
     * Case for when contact found.
     */
    @Test
    void testUpdateContact_ContactFound() {
        Long id = 1L;
        Contact originalContact = mock(Contact.class);
        Contact updatedContact = new Contact("def", "b1", null, null, "state", "zip", "country", "us", null);

        when(contactRepositoryMock.findById(id)).thenReturn(Optional.of(originalContact));
        when(contactRepositoryMock.save(any(Contact.class))).thenReturn(updatedContact);

        underTest.updateContact(id, updatedContact);

        verify(contactRepositoryMock, times(1)).findById(id);
        verify(originalContact, times(1)).setName(updatedContact.getName());
        verify(originalContact, times(1)).setEmailAddress(updatedContact.getEmailAddress());
        verify(originalContact, times(1)).setAddressLine1(updatedContact.getAddressLine1());
        verify(originalContact, times(1)).setAddressLine2(updatedContact.getAddressLine2());
        verify(originalContact, times(1)).setCity(updatedContact.getCity());
        verify(originalContact, times(1)).setState(updatedContact.getState());
        verify(originalContact, times(1)).setPostalCode(updatedContact.getPostalCode());
        verify(originalContact, times(1)).setCountry(updatedContact.getCountry());
        verify(contactRepositoryMock, times(1)).save(any(Contact.class));

        verifyNoMoreInteractions(contactRepositoryMock);
        verifyNoMoreInteractions(originalContact);
    }

    /**
     * Test for {@link ContactsService#updateContact(Long, Contact)}.
     * <p>
     * Case for when contact not found.
     */
    @Test
    void testUpdateAccount_ContactNotFound() {
        Long id = 2L;

        when(contactRepositoryMock.findById(id)).thenReturn(Optional.empty());

        try {
            underTest.updateContact(id, any(Contact.class));
            fail();
        } catch (Exception ex) {
            assertTrue(ex instanceof ContactNotFoundException);
            assertEquals(String.format("Could not find contact %d", id), ex.getMessage());
        }

        verify(contactRepositoryMock, times(1)).findById(id);
        verify(contactRepositoryMock, times(0)).save(any(Contact.class));
    }

    /**
     * Compare expected vs actual list of contacts.
     *
     * @param expected
     *         expected
     * @param actual
     *         actual
     */
    private void verifyContacts(List<Contact> expected, List<Contact> actual) {
        assertEquals(expected.size(), actual.size());
        IntStream.range(0, expected.size())
                .forEach(i -> {
                    assertEquals(expected.get(i).getName(), actual.get(i).getName());
                    assertEquals(expected.get(i).getEmailAddress(), actual.get(i).getEmailAddress());
                    assertEquals(expected.get(i).getAddressLine1(), actual.get(i).getAddressLine1());
                    assertEquals(expected.get(i).getAddressLine2(), actual.get(i).getAddressLine2());
                    assertEquals(expected.get(i).getCity(), actual.get(i).getCity());
                    assertEquals(expected.get(i).getState(), actual.get(i).getState());
                    assertEquals(expected.get(i).getPostalCode(), actual.get(i).getPostalCode());
                    assertEquals(expected.get(i).getCountry(), actual.get(i).getCountry());
                    assertEquals(expected.get(i).getAccount(), actual.get(i).getAccount());
                });
    }

}