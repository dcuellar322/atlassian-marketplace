package com.dac.atlassianmarketplace.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import com.dac.atlassianmarketplace.repository.entity.Account;
import com.dac.atlassianmarketplace.service.AccountsService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;

/**
 * Test class for {@link AccountsController}.
 *
 * @author dac
 */
@SuppressWarnings("unused")
@WebMvcTest(value = AccountsController.class)
class AccountsControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private DefaultMockMvcBuilder mockMvcBuilder;

    @MockBean
    private AccountsService accountsService;

    @BeforeEach
    void setUp() {
        initMocks(this);
        mockMvc = mockMvcBuilder.build();
    }

    /**
     * Test for {@link AccountsController#createAccount(Account)}.
     *
     * @throws Exception
     *         exception
     */
    @Test
    void testCreateAccount() throws Exception {
        Account expectedAccount = new Account("abc", "a1", "a2", "city", "state", "zip", "country");

        when(accountsService.createAccount(any(Account.class))).thenReturn(expectedAccount);

        mockMvc.perform(post("/accounts")
                .contentType(APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(expectedAccount)))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "http://localhost/accounts/0"));

        verify(accountsService).createAccount(any(Account.class));
        verifyNoMoreInteractions(accountsService);
    }

    /**
     * Test for {@link AccountsController#getAccount(Long)}.
     * <p>
     * Case for when account found.
     *
     * @throws Exception
     *         exception
     */
    @Test
    void testGetAccount_AccountFound() throws Exception {
        Long id = 1L;
        Account expectedAccount = new Account("abc", "a1", "a2", "city", "state", "zip", "country");

        when(accountsService.getAccount(id)).thenReturn(Optional.of(expectedAccount));

        mockMvc.perform(get("/accounts/{id}", id)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(new ObjectMapper().writeValueAsString(expectedAccount)));

        verify(accountsService).getAccount(id);
        verifyNoMoreInteractions(accountsService);
    }

    /**
     * Test for {@link AccountsController#getAccount(Long)}.
     * <p>
     * Case for when account not found.
     *
     * @throws Exception
     *         exception
     */
    @Test
    void testGetAccount_AccountNotFound() throws Exception {
        Long id = 2L;

        when(accountsService.getAccount(id)).thenReturn(Optional.empty());

        mockMvc.perform(get("/accounts/{id}", id)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(content().string(String.format("Could not find account %d", id)));

        verify(accountsService).getAccount(id);
        verifyNoMoreInteractions(accountsService);
    }

    /**
     * Test for {@link AccountsController#getAccounts()}.
     * <p>
     * Case for when accounts exist.
     *
     * @throws Exception
     *         exception
     */
    @Test
    void testGetAccounts() throws Exception {
        List<Account> expectedAccounts = Arrays.asList(
                new Account("abc", "a1", "a2", "city", "state", "zip", "country"),
                new Account("def", "b1", null, "c1", "state", "zip", "country")
        );

        when(accountsService.getAccounts()).thenReturn(expectedAccounts);

        mockMvc.perform(get("/accounts")
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(new ObjectMapper().writeValueAsString(expectedAccounts)));

        verify(accountsService).getAccounts();
        verifyNoMoreInteractions(accountsService);
    }

    /**
     * Test for {@link AccountsController#getAccounts()}.
     * <p>
     * Case for no accounts exist.
     *
     * @throws Exception
     *         exception
     */
    @Test
    void testGetAccounts_NoAccounts() throws Exception {
        List<Account> expectedAccounts = new ArrayList<>();
        when(accountsService.getAccounts()).thenReturn(expectedAccounts);

        mockMvc.perform(get("/accounts")
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(new ObjectMapper().writeValueAsString(expectedAccounts)));

        verify(accountsService).getAccounts();
        verifyNoMoreInteractions(accountsService);
    }

    /**
     * Test for {@link AccountsController#updateAccount(Account, Long)}.
     *
     * @throws Exception
     *         exception
     */
    @Test
    void testUpdateAccount() throws Exception {
        Long id = 1L;
        Account account = new Account("abc", "a1", "a2", "city", "state", "zip", "country");

        doNothing().when(accountsService).updateAccount(eq(id), any(Account.class));

        mockMvc.perform(put("/accounts/{id}", id)
                .contentType(APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(account)))
                .andExpect(status().isNoContent());

        verify(accountsService).updateAccount(eq(id), any(Account.class));
        verifyNoMoreInteractions(accountsService);
    }
}