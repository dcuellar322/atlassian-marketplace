package com.dac.atlassianmarketplace.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import com.dac.atlassianmarketplace.exception.AccountNotFoundException;
import com.dac.atlassianmarketplace.exception.ContactNotFoundException;
import com.dac.atlassianmarketplace.repository.entity.Contact;
import com.dac.atlassianmarketplace.service.ContactsService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;

/**
 * Test class for {@link ContactsController}.
 *
 * @author dac
 */
@SuppressWarnings("unused")
@WebMvcTest(value = ContactsController.class)
class ContactsControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private DefaultMockMvcBuilder mockMvcBuilder;

    @MockBean
    private ContactsService contactsService;

    @BeforeEach
    void setUp() {
        initMocks(this);
        mockMvc = mockMvcBuilder.build();
    }

    /**
     * Test for {@link ContactsController#associateContactWithAccount(Long, Long)}.
     * <p>
     * Case for when account not found.
     *
     * @throws Exception
     *         exception
     */
    @Test
    void testAssociateContactWithAccount_AccountNotFound() throws Exception {
        Long id = 1L;
        Long accountId = 99L;

        AccountNotFoundException exception = new AccountNotFoundException(accountId);

        doThrow(exception).when(contactsService).associateContactWithAccount(id, accountId);

        mockMvc.perform(put("/accounts/{accountId}/contacts/{id}", accountId, id)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(content().string(String.format("Could not find account %d", accountId)));

        verify(contactsService).associateContactWithAccount(id, accountId);
        verifyNoMoreInteractions(contactsService);
    }

    /**
     * Test for {@link ContactsController#associateContactWithAccount(Long, Long)}.
     * <p>
     * Case for when contact not found.
     *
     * @throws Exception
     *         exception
     */
    @Test
    void testAssociateContactWithAccount_ContactNotFound() throws Exception {
        Long id = 1L;
        Long accountId = 99L;

        ContactNotFoundException exception = new ContactNotFoundException(id);

        doThrow(exception).when(contactsService).associateContactWithAccount(id, accountId);

        mockMvc.perform(put("/accounts/{accountId}/contacts/{id}", accountId, id)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(content().string(String.format("Could not find contact %d", id)));

        verify(contactsService).associateContactWithAccount(id, accountId);
        verifyNoMoreInteractions(contactsService);
    }

    /**
     * Test for {@link ContactsController#associateContactWithAccount(Long, Long)}.
     * <p>
     * Case for when account and contact found.
     *
     * @throws Exception
     *         exception
     */
    @Test
    void testAssociateContactWithAccount_AccountAndContactFound() throws Exception {
        Long id = 1L;
        Long accountId = 99L;

        doNothing().when(contactsService).associateContactWithAccount(id, accountId);

        mockMvc.perform(put("/accounts/{accountId}/contacts/{id}", accountId, id)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isNoContent());

        verify(contactsService).associateContactWithAccount(id, accountId);
        verifyNoMoreInteractions(contactsService);
    }

    /**
     * Test for {@link ContactsController#createContact(Contact)}.
     *
     * @throws Exception
     *         exception
     */
    @Test
    void testCreateContact() throws Exception {
        Contact expectedContact = new Contact("abc", "a1@test.com", null, null, "state", "zip", "country", "us", null);

        when(contactsService.createContact(any(Contact.class))).thenReturn(expectedContact);

        mockMvc.perform(post("/contacts")
                .contentType(APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(expectedContact)))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "http://localhost/contacts/0"));

        verify(contactsService).createContact(any(Contact.class));
        verifyNoMoreInteractions(contactsService);
    }

    /**
     * Test for {@link ContactsController#getContact(Long)}.
     * <p>
     * Case for when contact found.
     *
     * @throws Exception
     *         exception
     */
    @Test
    void testGetContact_ContactFound() throws Exception {
        Long id = 1L;
        Contact expectedContact = new Contact("abc", "a1@test.com", null, null, "state", "zip", "country", "us", null);

        when(contactsService.getContact(id)).thenReturn(Optional.of(expectedContact));

        mockMvc.perform(get("/contacts/{id}", id)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(new ObjectMapper().writeValueAsString(expectedContact)));

        verify(contactsService).getContact(id);
        verifyNoMoreInteractions(contactsService);
    }

    /**
     * Test for {@link ContactsController#getContact(Long)}.
     * <p>
     * Case for when contact not found.
     *
     * @throws Exception
     *         exception
     */
    @Test
    void testGetContact_ContactNotFound() throws Exception {
        Long id = 2L;

        when(contactsService.getContact(id)).thenReturn(Optional.empty());

        mockMvc.perform(get("/contacts/{id}", id)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(content().string(String.format("Could not find contact %d", id)));

        verify(contactsService).getContact(id);
        verifyNoMoreInteractions(contactsService);
    }

    /**
     * Test for {@link ContactsController#getContacts()}.
     * <p>
     * Case for when contacts exist.
     *
     * @throws Exception
     *         exception
     */
    @Test
    void testGetContacts() throws Exception {
        List<Contact> expectedContacts = Arrays.asList(
                new Contact("abc", "a1@test.com", null, null, "state", "zip", "country", "us", null),
                new Contact("def", "b1@test.com", null, null, "state", "zip", "country", "us", null)
        );

        when(contactsService.getContacts()).thenReturn(expectedContacts);

        mockMvc.perform(get("/contacts")
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(new ObjectMapper().writeValueAsString(expectedContacts)));

        verify(contactsService).getContacts();
        verifyNoMoreInteractions(contactsService);
    }

    /**
     * Test for {@link ContactsController#getContacts()}.
     * <p>
     * Case for no contacts exist.
     *
     * @throws Exception
     *         exception
     */
    @Test
    void testGetContacts_NoContacts() throws Exception {
        List<Contact> expectedContacts = new ArrayList<>();
        when(contactsService.getContacts()).thenReturn(expectedContacts);

        mockMvc.perform(get("/contacts")
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(new ObjectMapper().writeValueAsString(expectedContacts)));

        verify(contactsService).getContacts();
        verifyNoMoreInteractions(contactsService);
    }

    /**
     * Test for {@link ContactsController#getContactsByAccountId(Long)}.
     * <p>
     * Case for when account not found.
     *
     * @throws Exception
     *         exception
     */
    @Test
    void testGetContactsByAccountId_AccountNotFound() throws Exception {
        Long accountId = 99L;

        AccountNotFoundException exception = new AccountNotFoundException(accountId);

        doThrow(exception).when(contactsService).getContactsByAccountId(accountId);

        mockMvc.perform(get("/accounts/{accountId}/contacts", accountId)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(content().string(String.format("Could not find account %d", accountId)));

        verify(contactsService).getContactsByAccountId(accountId);
        verifyNoMoreInteractions(contactsService);
    }

    /**
     * Test for {@link ContactsController#getContactsByAccountId(Long)}.
     * <p>
     * Case for when account has contacts.
     *
     * @throws Exception
     *         exception
     */
    @Test
    void testGetContactsByAccountId_AccountFoundWithContacts() throws Exception {
        Long accountId = 99L;
        List<Contact> expectedContacts = Arrays.asList(
                new Contact("abc", "a1@test.com", null, null, "state", "zip", "country", "us", null),
                new Contact("def", "b1@test.com", null, null, "state", "zip", "country", "us", null)
        );

        when(contactsService.getContactsByAccountId(accountId)).thenReturn(expectedContacts);

        mockMvc.perform(get("/accounts/{accountId}/contacts", accountId)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(new ObjectMapper().writeValueAsString(expectedContacts)));

        verify(contactsService).getContactsByAccountId(accountId);
        verifyNoMoreInteractions(contactsService);
    }

    /**
     * Test for {@link ContactsController#getContactsByAccountId(Long)}.
     * <p>
     * Case for when account has no contacts.
     *
     * @throws Exception
     *         exception
     */
    @Test
    void testGetContactsByAccountId_AccountFoundWithNoContacts() throws Exception {
        Long accountId = 99L;
        List<Contact> expectedContacts = new ArrayList<>();

        when(contactsService.getContactsByAccountId(accountId)).thenReturn(expectedContacts);

        mockMvc.perform(get("/accounts/{accountId}/contacts", accountId)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(new ObjectMapper().writeValueAsString(expectedContacts)));

        verify(contactsService).getContactsByAccountId(accountId);
        verifyNoMoreInteractions(contactsService);
    }

    /**
     * Test for {@link ContactsController#getContactByIdAndAccountId(Long, Long)}.
     * <p>
     * Case for when account is not found.
     *
     * @throws Exception
     *         exception
     */
    @Test
    void testGetContactByIdAndAccountId_AccountNotFound() throws Exception {
        Long id = 1L;
        Long accountId = 99L;

        AccountNotFoundException exception = new AccountNotFoundException(accountId);

        doThrow(exception).when(contactsService).getContactByIdAndAccountId(id, accountId);

        mockMvc.perform(get("/accounts/{accountId}/contacts/{id}", accountId, id)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(content().string(String.format("Could not find account %d", accountId)));

        verify(contactsService).getContactByIdAndAccountId(id, accountId);
        verifyNoMoreInteractions(contactsService);
    }

    /**
     * Test for {@link ContactsController#getContactByIdAndAccountId(Long, Long)}.
     * <p>
     * Case for when contact is not found.
     *
     * @throws Exception
     *         exception
     */
    @Test
    void testGetContactByIdAndAccountId_ContactNotFound() throws Exception {
        Long id = 1L;
        Long accountId = 99L;

        when(contactsService.getContactByIdAndAccountId(id, accountId)).thenReturn(Optional.empty());

        mockMvc.perform(get("/accounts/{accountId}/contacts/{id}", accountId, id)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(content().string(String.format("Could not find contact %d", id)));

        verify(contactsService).getContactByIdAndAccountId(id, accountId);
        verifyNoMoreInteractions(contactsService);
    }

    /**
     * Test for {@link ContactsController#getContactByIdAndAccountId(Long, Long)}.
     * <p>
     * Case for when account and contact are found.
     *
     * @throws Exception
     *         exception
     */
    @Test
    void testGetContactByIdAndAccountId_AccountAndContactFound() throws Exception {
        Long id = 1L;
        Long accountId = 99L;
        Contact expectedContact = new Contact("abc", "a1@test.com", null, null, "state", "zip", "country", "us", null);

        when(contactsService.getContactByIdAndAccountId(id, accountId)).thenReturn(Optional.of(expectedContact));

        mockMvc.perform(get("/accounts/{accountId}/contacts/{id}", accountId, id)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(new ObjectMapper().writeValueAsString(expectedContact)));

        verify(contactsService).getContactByIdAndAccountId(id, accountId);
        verifyNoMoreInteractions(contactsService);
    }

    /**
     * Test for {@link ContactsController#updateContact(Contact, Long)}.
     *
     * @throws Exception
     *         exception
     */
    @Test
    void testUpdateContact() throws Exception {
        Long id = 1L;
        Contact contact = new Contact("abc", "a1@test.com", null, null, "state", "zip", "country", "us", null);

        doNothing().when(contactsService).updateContact(eq(id), any(Contact.class));

        mockMvc.perform(put("/contacts/{id}", id)
                .contentType(APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(contact)))
                .andExpect(status().isNoContent());

        verify(contactsService).updateContact(eq(id), any(Contact.class));
        verifyNoMoreInteractions(contactsService);
    }
}