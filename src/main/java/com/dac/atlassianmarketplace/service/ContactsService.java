package com.dac.atlassianmarketplace.service;

import com.dac.atlassianmarketplace.exception.AccountNotFoundException;
import com.dac.atlassianmarketplace.exception.ContactNotFoundException;
import com.dac.atlassianmarketplace.repository.AccountRepository;
import com.dac.atlassianmarketplace.repository.ContactRepository;
import com.dac.atlassianmarketplace.repository.entity.Account;
import com.dac.atlassianmarketplace.repository.entity.Contact;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Contacts Service for isolating logic between controller layer and repository layer.
 *
 * @author dac
 */
@Service
public class ContactsService {

    private ContactRepository contactRepository;

    private AccountRepository accountRepository;

    public ContactsService(ContactRepository contactRepository, AccountRepository accountRepository) {
        this.contactRepository = contactRepository;
        this.accountRepository = accountRepository;
    }

    /**
     * Associate the contact at given id with the account at given id.
     *
     * @param id
     *         contact id
     * @param accountId
     *         account id
     */
    public void associateContactWithAccount(Long id, Long accountId) {
        Account account = accountRepository.findById(accountId)
                .orElseThrow(() -> new AccountNotFoundException(accountId));

        contactRepository.findById(id).map(contact -> {
            contact.setAccount(account);

            return contactRepository.save(contact);
        }).orElseThrow(() -> new ContactNotFoundException(id));
    }

    /**
     * Creates the given contact.
     *
     * @param contact
     *         contact to create
     * @return created contact, which includes id that was generated
     */
    public Contact createContact(Contact contact) {
        return contactRepository.save(contact);
    }

    /**
     * Get contact for given id.
     *
     * @param id
     *         contact id
     * @return contact if found for given id
     */
    public Optional<Contact> getContact(Long id) {
        return contactRepository.findById(id);
    }

    /**
     * Get all contacts.
     *
     * @return list of contacts
     */
    public List<Contact> getContacts() {
        return contactRepository.findAll();
    }

    /**
     * Get list of contacts for given account id.
     *
     * @param accountId
     *         account id
     * @return list of contacts associated with given account id
     */
    public List<Contact> getContactsByAccountId(Long accountId) {
        if (!accountRepository.existsById(accountId)) {
            throw new AccountNotFoundException(accountId);
        }
        return contactRepository.findByAccountId(accountId);
    }

    /**
     * Get contact for given id and associated with given account id
     *
     * @param id
     *         contact id
     * @param accountId
     *         account id
     * @return contact if found and associated with account
     */
    public Optional<Contact> getContactByIdAndAccountId(Long id, Long accountId) {
        if (!accountRepository.existsById(accountId)) {
            throw new AccountNotFoundException(accountId);
        }
        return contactRepository.findByIdAndAccountId(id, accountId);
    }

    /**
     * Update the contact at given id with updated object.
     *
     * @param id
     *         contact id to update
     * @param updatedContact
     *         updated contact
     */
    public void updateContact(Long id, Contact updatedContact) {
        contactRepository.findById(id).map(contact -> {
            contact.setName(updatedContact.getName());
            contact.setEmailAddress(updatedContact.getEmailAddress());
            contact.setAddressLine1(updatedContact.getAddressLine1());
            contact.setAddressLine2(updatedContact.getAddressLine2());
            contact.setCity(updatedContact.getCity());
            contact.setState(updatedContact.getState());
            contact.setPostalCode(updatedContact.getPostalCode());
            contact.setCountry(updatedContact.getCountry());

            return contactRepository.save(contact);
        }).orElseThrow(() -> new ContactNotFoundException(id));
    }

}
