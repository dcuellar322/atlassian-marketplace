package com.dac.atlassianmarketplace.service;

import com.dac.atlassianmarketplace.exception.AccountNotFoundException;
import com.dac.atlassianmarketplace.repository.AccountRepository;
import com.dac.atlassianmarketplace.repository.entity.Account;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Accounts Service for isolating logic between controller layer and repository layer.
 *
 * @author dac
 */
@Service
public class AccountsService {

    private AccountRepository accountRepository;

    public AccountsService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    /**
     * Creates the given account.
     *
     * @param account
     *         account to create
     * @return created account, which includes id that was generated
     */
    public Account createAccount(Account account) {
        return accountRepository.save(account);
    }

    /**
     * Get account for given id.
     *
     * @param id
     *         account id
     * @return account if found for given id
     */
    public Optional<Account> getAccount(Long id) {
        return accountRepository.findById(id);
    }

    /**
     * Get all accounts.
     *
     * @return list of accounts
     */
    public List<Account> getAccounts() {
        return accountRepository.findAll();
    }

    /**
     * Update the account at given id with updated object.
     *
     * @param id
     *         account id to update
     * @param updatedAccount
     *         updated account
     */
    public void updateAccount(Long id, Account updatedAccount) {
        accountRepository.findById(id).map(account -> {
            account.setCompanyName(updatedAccount.getCompanyName());
            account.setAddressLine1(updatedAccount.getAddressLine1());
            account.setAddressLine2(updatedAccount.getAddressLine2());
            account.setCity(updatedAccount.getCity());
            account.setState(updatedAccount.getState());
            account.setPostalCode(updatedAccount.getPostalCode());
            account.setCountry(updatedAccount.getCountry());

            return accountRepository.save(account);
        }).orElseThrow(() -> new AccountNotFoundException(id));
    }
}
