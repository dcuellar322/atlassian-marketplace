package com.dac.atlassianmarketplace.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import com.dac.atlassianmarketplace.exception.AccountNotFoundException;
import com.dac.atlassianmarketplace.repository.entity.Account;
import com.dac.atlassianmarketplace.service.AccountsService;
import java.net.URI;
import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 * Accounts REST Controller.
 *
 * @author dac
 */
@RestController
@SuppressWarnings("unused")
public class AccountsController {

    private AccountsService accountsService;

    public AccountsController(AccountsService accountsService) {
        this.accountsService = accountsService;
    }

    /**
     * Receives Account in request body and creates account.
     * <p>
     * Returns "application/json" content-type and 201 CREATED http status code.
     *
     * @param account
     *         account to create
     * @return resource for created account
     */
    @PostMapping("/accounts")
    public ResponseEntity<Account> createAccount(@RequestBody Account account) {
        Account newAccount = accountsService.createAccount(account);
        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(newAccount.getId())
                .toUri();
        return ResponseEntity
                .created(uri)
                .contentType(APPLICATION_JSON)
                .build();
    }

    /**
     * Retrieves account for given id.
     * <p>
     * Returns "application/json" content-type and 200 OK http status code.
     *
     * @param id
     *         account id
     * @return account
     * @throws AccountNotFoundException
     *         if account is not found with given id
     */
    @GetMapping("/accounts/{id}")
    public ResponseEntity<Account> getAccount(@PathVariable Long id) {
        return ResponseEntity
                .ok(accountsService.getAccount(id)
                        .orElseThrow(() -> new AccountNotFoundException(id)));
    }

    /**
     * Retrieves accounts.
     * <p>
     * Returns "application/json" content-type and 200 OK http status code.
     *
     * @return list of accounts
     */
    @GetMapping("/accounts")
    public ResponseEntity<List<Account>> getAccounts() {
        return ResponseEntity.ok(accountsService.getAccounts());
    }

    /**
     * Updates account at given id with account in the request body.
     * <p>
     * Returns "application/json" content-type and 204 NO_CONTENT http status code.
     *
     * @throws AccountNotFoundException
     *         if account not found at given id
     */
    @PutMapping("/accounts/{id}")
    public ResponseEntity<Void> updateAccount(@RequestBody Account account, @PathVariable Long id) {
        accountsService.updateAccount(id, account);

        return ResponseEntity
                .noContent()
                .build();
    }
}
