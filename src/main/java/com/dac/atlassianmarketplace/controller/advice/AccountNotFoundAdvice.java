package com.dac.atlassianmarketplace.controller.advice;

import com.dac.atlassianmarketplace.exception.AccountNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Account Not Found controller advice.
 * <p>
 * Wrap AccountNotFoundException to HTTP Status Code 404 and put exception message in Response Body.
 *
 * @author dac
 */
@ControllerAdvice
@SuppressWarnings("unused")
public class AccountNotFoundAdvice {
    @ResponseBody
    @ExceptionHandler(AccountNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String accountNotFoundHandler(AccountNotFoundException ex) {
        return ex.getMessage();
    }
}
