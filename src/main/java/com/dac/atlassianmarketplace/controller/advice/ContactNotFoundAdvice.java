package com.dac.atlassianmarketplace.controller.advice;

import com.dac.atlassianmarketplace.exception.ContactNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Contact Not Found controller advice.
 * <p>
 * Wrap ContactNotFoundException to HTTP Status Code 404 and put exception message in Response Body.
 *
 * @author dac
 */
@ControllerAdvice
@SuppressWarnings("unused")
public class ContactNotFoundAdvice {
    @ResponseBody
    @ExceptionHandler(ContactNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String contactNotFoundHandler(ContactNotFoundException ex) {
        return ex.getMessage();
    }
}
