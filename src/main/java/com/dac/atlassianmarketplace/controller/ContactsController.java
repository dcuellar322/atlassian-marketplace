package com.dac.atlassianmarketplace.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import com.dac.atlassianmarketplace.exception.ContactNotFoundException;
import com.dac.atlassianmarketplace.repository.entity.Account;
import com.dac.atlassianmarketplace.repository.entity.Contact;
import com.dac.atlassianmarketplace.service.ContactsService;
import java.net.URI;
import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 * Contacts REST Controller.
 *
 * @author dac
 */
@RestController
@SuppressWarnings("unused")
public class ContactsController {

    private ContactsService contactsService;

    public ContactsController(ContactsService contactsService) {
        this.contactsService = contactsService;
    }

    /**
     * Associates the given contact id with given account id.
     * <p>
     * Returns "application/json" content-type and 204 NO_CONTENT http status code.
     *
     * @param accountId
     *         account id
     * @param id
     *         contact id
     */
    @PutMapping("/accounts/{accountId}/contacts/{id}")
    public ResponseEntity<Void> associateContactWithAccount(@PathVariable Long accountId, @PathVariable Long id) {
        contactsService.associateContactWithAccount(id, accountId);

        return ResponseEntity
                .noContent()
                .build();
    }

    /**
     * Receives Contact in request body and creates contact.
     * <p>
     * Returns "application/json" content-type and 201 CREATED http status code.
     *
     * @param contact
     *         contact to create
     * @return resource for created contact
     */
    @PostMapping("/contacts")
    public ResponseEntity<Account> createContact(@RequestBody Contact contact) {
        Contact newContact = contactsService.createContact(contact);
        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(newContact.getId())
                .toUri();
        return ResponseEntity
                .created(uri)
                .contentType(APPLICATION_JSON)
                .build();
    }

    /**
     * Retrieves contact for given id.
     * <p>
     * Returns "application/json" content-type and 200 OK http status code.
     *
     * @param id
     *         contact id
     * @return contact
     * @throws ContactNotFoundException
     *         if contact is not found with given id
     */
    @GetMapping("/contacts/{id}")
    public ResponseEntity<Contact> getContact(@PathVariable Long id) {
        return ResponseEntity
                .ok(contactsService.getContact(id)
                        .orElseThrow(() -> new ContactNotFoundException(id)));
    }

    /**
     * Retrieves contacts.
     * <p>
     * Returns "application/json" content-type and 200 OK http status code.
     *
     * @return list of contacts
     */
    @GetMapping("/contacts")
    public ResponseEntity<List<Contact>> getContacts() {
        return ResponseEntity.ok(contactsService.getContacts());
    }

    /**
     * Retrieves contacts for given account id.
     * <p>
     * Returns "application/json" content-type and 200 OK http status code.
     *
     * @return list of contacts
     * @throws com.dac.atlassianmarketplace.exception.AccountNotFoundException
     *         if account not found at given id
     */
    @GetMapping("/accounts/{accountId}/contacts")
    public ResponseEntity<List<Contact>> getContactsByAccountId(@PathVariable Long accountId) {
        return ResponseEntity.ok(contactsService.getContactsByAccountId(accountId));
    }

    /**
     * Retrieves contact for given id and for given account id.
     * <p>
     * Returns "application/json" content-type and 200 OK http status code.
     *
     * @return contact
     * @throws com.dac.atlassianmarketplace.exception.AccountNotFoundException
     *         if account not found at given id
     * @throws ContactNotFoundException
     *         if contact not associated with given account id
     */
    @GetMapping("/accounts/{accountId}/contacts/{id}")
    public ResponseEntity<Contact> getContactByIdAndAccountId(@PathVariable Long accountId, @PathVariable Long id) {
        return ResponseEntity
                .ok(contactsService.getContactByIdAndAccountId(id, accountId)
                        .orElseThrow(() -> new ContactNotFoundException(id)));
    }

    /**
     * Updates contact at given id with contact in the request body.
     * <p>
     * Returns "application/json" content-type and 204 NO_CONTENT http status code.
     *
     * @throws ContactNotFoundException
     *         if contact not found at given id
     */
    @PutMapping("/contacts/{id}")
    public ResponseEntity<Void> updateContact(@RequestBody Contact contact, @PathVariable Long id) {
        contactsService.updateContact(id, contact);

        return ResponseEntity
                .noContent()
                .build();
    }

}
