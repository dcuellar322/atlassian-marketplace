package com.dac.atlassianmarketplace.repository;

import com.dac.atlassianmarketplace.repository.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for interacting with Accounts table via JPA API.
 *
 * @author dac
 */
@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {
}
