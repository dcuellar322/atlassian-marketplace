package com.dac.atlassianmarketplace.repository;

import com.dac.atlassianmarketplace.repository.entity.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Repository for interacting with Contacts table via JPA API.
 *
 * @author dac
 */
@Repository
public interface ContactRepository extends JpaRepository<Contact, Long> {
    /**
     * Find contacts by account id.
     * <p>
     * Accounts to contacts represents a one-to-many relationship, and this allows fetching of contacts
     * that are linked to accounts.
     *
     * @param accountId
     *         account id
     * @return list of contacts associated with given account id
     */
    List<Contact> findByAccountId(Long accountId);

    /**
     * Find contact by id and account id.
     * <p>
     * Will only return a contact if that contact is associated with given account id.
     *
     * @param id
     *         contact id
     * @param accountId
     *         account id
     * @return contact if found for account id
     */
    Optional<Contact> findByIdAndAccountId(Long id, Long accountId);
}
