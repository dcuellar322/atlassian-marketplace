package com.dac.atlassianmarketplace.exception;

/**
 * Account Not Found Exception.
 * <p>
 * Wrapped RuntimeException to be thrown from lambdas whenever an account is not found.
 *
 * @author dac
 */
public class AccountNotFoundException extends RuntimeException {
    public AccountNotFoundException(Long id) {
        super(String.format("Could not find account %d", id));
    }
}
