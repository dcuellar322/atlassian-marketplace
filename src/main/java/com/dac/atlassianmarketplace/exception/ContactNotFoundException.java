package com.dac.atlassianmarketplace.exception;

/**
 * Contact Not Found Exception.
 * <p>
 * Wrapped RuntimeException to be thrown from lambdas whenever a contact is not found.
 *
 * @author dac
 */
public class ContactNotFoundException extends RuntimeException {
    public ContactNotFoundException(Long id) {
        super(String.format("Could not find contact %d", id));
    }
}
