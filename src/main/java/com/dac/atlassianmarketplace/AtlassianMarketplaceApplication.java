package com.dac.atlassianmarketplace;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Atlassian Marketplace Spring Boot Application.
 *
 * @author dac
 */
@SpringBootApplication
public class AtlassianMarketplaceApplication {

    public static void main(String[] args) {
        SpringApplication.run(AtlassianMarketplaceApplication.class, args);
    }

}
