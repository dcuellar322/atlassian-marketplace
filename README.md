# Atlassian Marketplace Demo Microservice #

The Atlassian Marketplace (MPAC) team needs a service that can store and serve basic information about Marketplace vendor accounts (companies who sell apps on marketplace.atlassian.com) and the contacts (people) who are associated with those accounts.

This project is meant to demo the technical requirements presented by the Business.

#### Technical requirements ####
* The web server should provide a REST API for the following objects and lists of objects in `application/JSON`
* Contact (`GET`/`POST`/`PUT`)
* Account (`GET`/`POST`/`PUT`)
* Account (`GET`)
    * Contacts
* The objects should be stored in a database
* Contacts should have the following attributes:
    * Name
    * Email address
    * Address line 1
    * Address line 2
    * City
    * State
    * Postal code
    * Country
* Accounts should have the following attributes:
    * Company name
    * Address line 1
    * Address line 2
    * City
    * State
    * Postal code
    * Country
* The account-contact relationship is one to many, and contacts may not necessarily be associated with an account.

#### Design Decisions & Assumptions ####
* SpringBoot was decided as the framework, given it's lightweight nature and out-of-box functionality (i.e. embedded tomcat)
* H2 database (small light-weight, out-of-box integration with SpringBoot, minimal configuration)
* Use file for H2 database instead of in-memory for persistence into Git
* Spring Data JPA
    * Leverages Java Persistence API via Spring Data and Hibernate
    * Typically, I would use Spring Data JDBC because I'm much more familiar with JDBC API but given the nature of this demo with limited model/entities that mostly CRUD operations I decided to use JPA/Hibernate. Also, as an opportunity to learn something new.
* No extra logging other than what SpringBoot provides out-of-box.
    * There isn't much business logic, and logging at Controller/Services levels seemed a little unnecessary and verbose.
    * If this demo would need to be productionalized, there would need to be logging.
* No monitoring.
    * SpringBoot offers the ability to use the Actuator library to provide some out-of-box monitoring but given nature of this demo project I decided to not include at this moment.
    * If this demo would need to be productionalized, there would need to be monitoring.
* No security.
    * SpringBoot offers lots in the area of Security, but again, given the nature of this demo I decided to not include at this moment.
    * If this demo would need to be productionalized, there would need to be security. Especially, since this service exposes Account and Contact information, we should need to ensure we know who is accessing the data via the REST endpoints.
* Decision for Contact to "own" the relationship with Account
    * Endpoints live in `ContactsController`
    * Therefore the relationship is `ManyToOne`.
    * As per the requirement, an Account can have many contacts. Also a contact may not have account.
    * My assumption is also that a Contact may only be associated one Account or none.
* Decision to add audit columns to entities

### Database ###
Application uses H2 in-memory database, but persists to file.

Changing H2 configuration, or changing to different data store can be done via `application.properties`.

H2 console can be accessed via http://localhost:8080/h2.

```
spring.datasource.url=jdbc:h2:file:./atlassian-marketplace
spring.datasource.username=sa
spring.datasource.password=
spring.datasource.driver-class-name=org.h2.Driver
```

### Build ###

```
$ ./gradlew bootJar
```

### Running Locally ###

You may run application using
```
$ ./gradlew bootRun
```

OR you may first build the JAR and execute the JAR to run application
```
$ ./gradlew bootJar
$ java -jar build/libs/atlassian-marketplace-0.0.1-SNAPSHOT.jar
```

### Test ###

```
$ ./gradlew test
```

### REST API Endpoints ###

#### Accounts ####

##### Create Account #####

Returns `201 CREATED` on success with created Resource in Location Header. See example below.
```
POST /accounts
Accept: application/json
Content-Type: application/json

{
	"companyName": "Atlassian",
	"addressLine1": "303 Colorado St",
	"addressLine2": null,
	"city": "Austin",
	"state": "TX",
	"postalCode": "78701",
	"country": "US"
}
```

Example:
```
$ curl -v -d "{\"companyName\": \"Atlassian\", \"addressLine1\": \"303 Colorado St\", \"addressLine2\": null, \"city\": \"Austin\", \"state\": \"TX\", \"postalCode\": \"78701\", \"country\": \"US\"}" -H "Content-Type: application/json" http://localhost:8080/accounts
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 8080 (#0)
> POST /accounts HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.54.0
> Accept: */*
> Content-Type: application/json
> Content-Length: 131
> 
* upload completely sent off: 131 out of 131 bytes
< HTTP/1.1 201 
< Location: http://localhost:8080/accounts/1
< Content-Type: application/json
< Content-Length: 0
< Date: Fri, 13 Mar 2020 14:34:16 GMT
< 
* Connection #0 to host localhost left intact
```

##### Get Account #####

Returns `200 OK` on success.

Return `404 NOT FOUND` if account not found.

```
GET /accounts/{id}
Accept: application/json
Content-Type: application/json
```

Example:
```
$ curl -v -H "Content-Type: application/json" http://localhost:8080/accounts/1
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 8080 (#0)
> GET /accounts/1 HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.54.0
> Accept: */*
> Content-Type: application/json
> 
< HTTP/1.1 200 
< Content-Type: application/json
< Transfer-Encoding: chunked
< Date: Fri, 13 Mar 2020 14:41:06 GMT
< 
* Connection #0 to host localhost left intact
{"id":1,"companyName":"Atlassian","addressLine1":"303 Colorado St","addressLine2":null,"city":"Austin","state":"TX","postalCode":"78701","country":"US"}

$ curl -v -H "Content-Type: application/json" http://localhost:8080/accounts/10
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 8080 (#0)
> GET /accounts/10 HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.54.0
> Accept: */*
> Content-Type: application/json
> 
< HTTP/1.1 404 
< Content-Type: text/plain;charset=UTF-8
< Content-Length: 25
< Date: Fri, 13 Mar 2020 14:41:34 GMT
< 
* Connection #0 to host localhost left intact
Could not find account 10
```

##### Get Accounts #####

Returns `200 OK` on success.

```
GET /accounts
Accept: application/json
Content-Type: application/json
```

Example:
```
$ curl -v -H "Content-Type: application/json" http://localhost:8080/accounts
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 8080 (#0)
> GET /accounts HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.54.0
> Accept: */*
> Content-Type: application/json
> 
< HTTP/1.1 200 
< Content-Type: application/json
< Transfer-Encoding: chunked
< Date: Fri, 13 Mar 2020 14:42:28 GMT
< 
* Connection #0 to host localhost left intact
[{"id":1,"companyName":"Atlassian","addressLine1":"303 Colorado St","addressLine2":null,"city":"Austin","state":"TX","postalCode":"78701","country":"US"},{"id":2,"companyName":"Google","addressLine1":"500 W 2nd St","addressLine2":"Ste. 2900","city":"Austin","state":"TX","postalCode":"78701","country":"US"}]
```

##### Update Account #####

Returns `204 NO CONTENT` on success. 

Returns `404 NOT FOUND` if account not found
```
PUT /accounts/{id}
Accept: application/json
Content-Type: application/json

{
	"companyName": "Atlassian",
	"addressLine1": "303 Colorado St",
	"addressLine2": null,
	"city": "Austin",
	"state": "Texas",
	"postalCode": "78701",
	"country": "US"
}
```

Example:
```
$ curl -v -X PUT -d "{\"companyName\": \"Atlassian\", \"addressLine1\": \"303 Colorado St\", \"addressLine2\": null, \"city\": \"Austin\", \"state\": \"Texas\", \"postalCode\": \"78701\", \"country\": \"US\"}" -H "Content-Type: application/json" http://localhost:8080/accounts/1
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 8080 (#0)
> PUT /accounts/1 HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.54.0
> Accept: */*
> Content-Type: application/json
> Content-Length: 139
> 
* upload completely sent off: 139 out of 139 bytes
< HTTP/1.1 204 
< Date: Fri, 13 Mar 2020 14:46:15 GMT
< 
* Connection #0 to host localhost left intact

$ curl -v -X PUT -d "{\"companyName\": \"Atlassian\", \"addressLine1\": \"303 Colorado St\", \"addressLine2\": null, \"city\": \"Austin\", \"state\": \"Texas\", \"postalCode\": \"78701\", \"country\": \"US\"}" -H "Content-Type: application/json" http://localhost:8080/accounts/10
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 8080 (#0)
> PUT /accounts/10 HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.54.0
> Accept: */*
> Content-Type: application/json
> Content-Length: 139
> 
* upload completely sent off: 139 out of 139 bytes
< HTTP/1.1 404 
< Content-Type: text/plain;charset=UTF-8
< Content-Length: 25
< Date: Fri, 13 Mar 2020 14:47:40 GMT
< 
* Connection #0 to host localhost left intact
Could not find account 10
```

#### Contacts ####

##### Associate Contact with Account #####

Returns `204 NO CONTENT` on success. 

Returns `404 NOT FOUND` if account not found OR if contact not found
```
PUT /accounts/{accountId}/contacts/{id}
Accept: application/json
Content-Type: application/json
```

Example:
```
$ curl -v -X PUT -H "Content-Type: application/json" http://localhost:8080/accounts/1/contacts/3
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 8080 (#0)
> PUT /accounts/2/contacts/7 HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.54.0
> Accept: */*
> Content-Type: application/json
> 
< HTTP/1.1 204 
< Date: Fri, 13 Mar 2020 15:34:39 GMT
< 
* Connection #0 to host localhost left intact

$ curl -v -X PUT -H "Content-Type: application/json" http://localhost:8080/accounts/10/contacts/3
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 8080 (#0)
> PUT /accounts/10/contacts/3 HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.54.0
> Accept: */*
> Content-Type: application/json
> 
< HTTP/1.1 404 
< Content-Type: text/plain;charset=UTF-8
< Content-Length: 25
< Date: Fri, 13 Mar 2020 15:37:28 GMT
< 
* Connection #0 to host localhost left intact
Could not find account 10

$ curl -v -X PUT -H "Content-Type: application/json" http://localhost:8080/accounts/1/contacts/20
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 8080 (#0)
> PUT /accounts/1/contacts/20 HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.54.0
> Accept: */*
> Content-Type: application/json
> 
< HTTP/1.1 404 
< Content-Type: text/plain;charset=UTF-8
< Content-Length: 25
< Date: Fri, 13 Mar 2020 15:37:49 GMT
< 
* Connection #0 to host localhost left intact
Could not find contact 20
```

##### Create Contact #####

Returns `201 CREATED` on success with created Resource in Location Header. See example below.
```
POST /contacts
Accept: application/json
Content-Type: application/json

{
	"name": "David Cuellar",
	"emailAddress": "david.a.cuellar@gmail.com",
	"addressLine1": "500 W 2nd St",
	"addressLine2": null,
	"city": "Austin",
	"state": "TX",
	"postalCode": "78701",
	"country": "US"
}
```

Example:
```
$ curl -v -d "{\"name\": \"David Cuellar\", \"emailAddress\": \"david.a.cuellar@gmail.com\", \"addressLine1\": \"500 W 2nd St\", \"addressLine2\": null, \"city\": \"Austin\", \"state\": \"TX\", \"postalCode\": \"78701\", \"country\": \"US\"}" -H "Content-Type: application/json" http://localhost:8080/contacts
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 8080 (#0)
> POST /contacts HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.54.0
> Accept: */*
> Content-Type: application/json
> Content-Length: 183
> 
* upload completely sent off: 183 out of 183 bytes
< HTTP/1.1 201 
< Location: http://localhost:8080/contacts/7
< Content-Type: application/json
< Content-Length: 0
< Date: Fri, 13 Mar 2020 15:11:50 GMT
< 
* Connection #0 to host localhost left intact
```

##### Get Contact #####

Returns `200 OK` on success.

Return `404 NOT FOUND` if account not found.

```
GET /contacts/{id}
Accept: application/json
Content-Type: application/json
```

Example:
```
$ curl -v -H "Content-Type: application/json" http://localhost:8080/contacts/3
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 8080 (#0)
> GET /contacts/3 HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.54.0
> Accept: */*
> Content-Type: application/json
> 
< HTTP/1.1 200 
< Content-Type: application/json
< Transfer-Encoding: chunked
< Date: Fri, 13 Mar 2020 15:07:44 GMT
< 
* Connection #0 to host localhost left intact
{"id":3,"name":"David Cuellar","emailAddress":"david.a.cuellar@gmail.com","addressLine1":"500 W 2nd St","addressLine2":null,"city":"Austin","state":"TX","postalCode":"78701","country":"US","account":{"id":1,"companyName":"Atlassian","addressLine1":"303 Colorado St","addressLine2":null,"city":"Austin","state":"TX","postalCode":"78701","country":"US"}}

$ curl -v -H "Content-Type: application/json" http://localhost:8080/contacts/20
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 8080 (#0)
> GET /contacts/20 HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.54.0
> Accept: */*
> Content-Type: application/json
> 
< HTTP/1.1 404 
< Content-Type: text/plain;charset=UTF-8
< Content-Length: 25
< Date: Fri, 13 Mar 2020 15:13:03 GMT
< 
* Connection #0 to host localhost left intact
Could not find contact 20
```

##### Get Contacts #####

Returns `200 OK` on success.

```
GET /contacts
Accept: application/json
Content-Type: application/json
```

Example:
```
$ curl -v -H "Content-Type: application/json" http://localhost:8080/contacts
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 8080 (#0)
> GET /contacts HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.54.0
> Accept: */*
> Content-Type: application/json
> 
< HTTP/1.1 200 
< Content-Type: application/json
< Transfer-Encoding: chunked
< Date: Fri, 13 Mar 2020 15:14:06 GMT
< 
* Connection #0 to host localhost left intact
[{"id":3,"name":"David Cuellar","emailAddress":"david.a.cuellar@gmail.com","addressLine1":"500 W 2nd St","addressLine2":null,"city":"Austin","state":"TX","postalCode":"78701","country":"US","account":{"id":1,"companyName":"Atlassian","addressLine1":"303 Colorado St","addressLine2":null,"city":"Austin","state":"TX","postalCode":"78701","country":"US"}},{"id":4,"name":"John Doe","emailAddress":"john.doe@gmail.com","addressLine1":"123 Main St","addressLine2":null,"city":"Any Town","state":"TX","postalCode":"11111","country":"US","account":{"id":1,"companyName":"Atlassian","addressLine1":"303 Colorado St","addressLine2":null,"city":"Austin","state":"TX","postalCode":"78701","country":"US"}},{"id":5,"name":"Jane Doe","emailAddress":"john.doe@gmail.com","addressLine1":"222 Shady Lane","addressLine2":null,"city":"Some City","state":"CA","postalCode":"90210","country":"US","account":null},{"id":6,"name":"Amy Cuellar","emailAddress":"amie.cuellar@gmail.com","addressLine1":"1702 Southwestern Trl","addressLine2":null,"city":"Round Rock","state":"TX","postalCode":"78664","country":"US","account":null}]
```

##### Get Contacts by Account ID #####

Returns `200 OK` on success.

Returns `404 NOT FOUND` if account not found.

```
GET /accounts/{accountId}/contacts
Accept: application/json
Content-Type: application/json
```

Example:
```
$ curl -v -H "Content-Type: application/json" http://localhost:8080/accounts/1/contacts
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 8080 (#0)
> GET /accounts/1/contacts HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.54.0
> Accept: */*
> Content-Type: application/json
> 
< HTTP/1.1 200 
< Content-Type: application/json
< Transfer-Encoding: chunked
< Date: Fri, 13 Mar 2020 15:26:40 GMT
< 
* Connection #0 to host localhost left intact
[{"id":3,"name":"David Cuellar","emailAddress":"david.a.cuellar@gmail.com","addressLine1":"500 W 2nd St","addressLine2":null,"city":"Austin","state":"Texas","postalCode":"78701","country":"US","account":{"id":1,"companyName":"Atlassian","addressLine1":"303 Colorado St","addressLine2":null,"city":"Austin","state":"TX","postalCode":"78701","country":"US"}},{"id":4,"name":"John Doe","emailAddress":"john.doe@gmail.com","addressLine1":"123 Main St","addressLine2":null,"city":"Any Town","state":"TX","postalCode":"11111","country":"US","account":{"id":1,"companyName":"Atlassian","addressLine1":"303 Colorado St","addressLine2":null,"city":"Austin","state":"TX","postalCode":"78701","country":"US"}}]

$ curl -v -H "Content-Type: application/json" http://localhost:8080/accounts/10/contacts
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 8080 (#0)
> GET /accounts/10/contacts HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.54.0
> Accept: */*
> Content-Type: application/json
> 
< HTTP/1.1 404 
< Content-Type: text/plain;charset=UTF-8
< Content-Length: 25
< Date: Fri, 13 Mar 2020 15:27:12 GMT
< 
* Connection #0 to host localhost left intact
Could not find account 10
```

##### Get Contact by ID for Account ID #####

Returns `200 OK` on success.

Returns `404 NOT FOUND` if account not found OR if contact is not associated with account.

```
GET /accounts/{accountId}/contacts/{id}
Accept: application/json
Content-Type: application/json
```

Example:
```
$ curl -v -H "Content-Type: application/json" http://localhost:8080/accounts/1/contacts/3
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 8080 (#0)
> GET /accounts/1/contacts/3 HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.54.0
> Accept: */*
> Content-Type: application/json
> 
< HTTP/1.1 200 
< Content-Type: application/json
< Transfer-Encoding: chunked
< Date: Fri, 13 Mar 2020 15:28:58 GMT
< 
* Connection #0 to host localhost left intact
{"id":3,"name":"David Cuellar","emailAddress":"david.a.cuellar@gmail.com","addressLine1":"500 W 2nd St","addressLine2":null,"city":"Austin","state":"Texas","postalCode":"78701","country":"US","account":{"id":1,"companyName":"Atlassian","addressLine1":"303 Colorado St","addressLine2":null,"city":"Austin","state":"TX","postalCode":"78701","country":"US"}}

$ curl -v -H "Content-Type: application/json" http://localhost:8080/accounts/10/contacts/20
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 8080 (#0)
> GET /accounts/10/contacts/20 HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.54.0
> Accept: */*
> Content-Type: application/json
> 
< HTTP/1.1 404 
< Content-Type: text/plain;charset=UTF-8
< Content-Length: 25
< Date: Fri, 13 Mar 2020 15:29:43 GMT
< 
* Connection #0 to host localhost left intact
Could not find account 10

$ curl -v -H "Content-Type: application/json" http://localhost:8080/accounts/2/contacts/3
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 8080 (#0)
> GET /accounts/2/contacts/3 HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.54.0
> Accept: */*
> Content-Type: application/json
> 
< HTTP/1.1 404 
< Content-Type: text/plain;charset=UTF-8
< Content-Length: 24
< Date: Fri, 13 Mar 2020 15:30:15 GMT
< 
* Connection #0 to host localhost left intact
Could not find contact 3
```

##### Update Contact #####

Returns `204 NO CONTENT` on success. 

Returns `404 NOT FOUND` if account not found
```
PUT /contacts/{id}
Accept: application/json
Content-Type: application/json

{
	"name": "David Cuellar",
	"emailAddress": "david.a.cuellar@gmail.com",
	"addressLine1": "500 W 2nd St",
	"addressLine2": null,
	"city": "Austin",
	"state": "TX",
	"postalCode": "78701",
	"country": "US"
}
```

Example:
```
$ curl -v -X PUT -d "{\"name\": \"David Cuellar\", \"emailAddress\": \"david.a.cuellar@gmail.com\", \"addressLine1\": \"500 W 2nd St\", \"addressLine2\": null, \"city\": \"Austin\", \"state\": \"Texas\", \"postalCode\": \"78701\", \"country\": \"US\"}" -H "Content-Type: application/json" http://localhost:8080/contacts/3
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 8080 (#0)
> PUT /contacts/3 HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.54.0
> Accept: */*
> Content-Type: application/json
> Content-Length: 200
> 
* upload completely sent off: 200 out of 200 bytes
< HTTP/1.1 204 
< Date: Fri, 13 Mar 2020 15:22:30 GMT
< 
* Connection #0 to host localhost left intact

$ curl -v -X PUT -d "{\"name\": \"David Cuellar\", \"emailAddress\": \"david.a.cuellar@gmail.com\", \"addressLine1\": \"500 W 2nd St\", \"addressLine2\": null, \"city\": \"Austin\", \"state\": \"Texas\", \"postalCode\": \"78701\", \"country\": \"US\"}" -H "Content-Type: application/json" http://localhost:8080/contacts/20
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 8080 (#0)
> PUT /contacts/20 HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.54.0
> Accept: */*
> Content-Type: application/json
> Content-Length: 200
> 
* upload completely sent off: 200 out of 200 bytes
< HTTP/1.1 404 
< Content-Type: text/plain;charset=UTF-8
< Content-Length: 25
< Date: Fri, 13 Mar 2020 15:23:22 GMT
< 
* Connection #0 to host localhost left intact
Could not find contact 20
```

### Repository Information ###

This repository is owned by David Cuellar.
